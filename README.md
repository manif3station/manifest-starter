# Manifestation #

Manifestation is open source and it is a project craftsman. Build project like playing building blocks.

### What is this repository for? ###

This git repo is a starting block of a new project.

### How do I get set up? ###

There are 2 ways.

1. git clone this repo and build docker image locally and start
```
cd ~; git clone https://manif3station@bitbucket.org/manif3station/manifest-starter.git
cd ~/manifest-starter; docker-compose build
Then carry on with step 2 ...
```
2. use the ready made docker image to start
```
docker run --rm -v ~/new-project:/here manifestation/start-here:v1.0 start-here
cd ~/new-project
./dock up -d
```

### Where are the plugins ###

You can create your own plugins or themes.

You can also use the ready made ones from here - https://github.com/manif3station

### How to install a plugin ###

1. Go to your project directory e.g. `cd ~/new-project`
2. `./dock exec web plugin-get <tgz file from the github plugin resp>`
2.1. e.g. Enable Dev Mode
2.2. `./dock exec web plugin-get -u https://github.com/manif3station/manifestation-plugin-dev-mode/raw/master/DeveloperMode-1.0.tgz`
2.3. After installed the plugin, need to reload your project `./dock up -d`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin - email: nuttier_dearies.0r@icloud.com
* Other community or team contact
