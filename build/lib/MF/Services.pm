package MF::Services;

use strict;
use warnings;

use Bread::Board ();
use MF::Utils qw(
  add_lib
  create_method
  defor
  listdirs
  mf_envs
);

my %env = mf_envs;

add_lib $env{MF_LIB_DIR};

my @lib_paths = ( $env{MF_CORE_SERVICE_DIR}, $env{MF_LIB_DIR} );

my $get_service_classname = sub {
    my ($full_path) = @_;

    foreach my $dir (@lib_paths) {
        if ( $full_path =~ m/\Q$dir\E/ ) {
            $full_path =~ s/$dir\///;
            $full_path =~ s/\.pm$//;
            return join '::', split /\//, $full_path;
        }
    }

    if ( $ENV{DEV} ) {
        warn "Could not find package name from $full_path";
    }
};

my $process_service_file;

$process_service_file = sub {
    my %options = @_;

    my $path = $options{path}
      or die "Missing path of the service file";

    add_lib "$path/";

    my $parent_container = $options{parent_container}
      or die "Missing parent_container";

    my $family_tree = $options{family_tree};

    my $service_file = "$path/$env{MF_SERVICE_FILE}";

    if ( !-f $service_file ) {
        return;
    }

    require $service_file;

    my $service_file_package_name = $get_service_classname->($service_file);

    my $services = $service_file_package_name->services
      or die "$service_file_package_name file is missing"
      . ' return list of services';

    die "The return payload from $service_file_package_name needs "
      . "to be HashRef. (? $services)"
      if !UNIVERSAL::isa( $services, 'HASH' );

    my $plugin;

    if ( $service_file_package_name->can('container_name') ) {
        $plugin = defor $service_file_package_name->container_name, '';
        $plugin =~ s/(\w+)/$1/g;
    }

    if ( !$plugin ) {
        ( undef, $plugin ) = ( $path =~ m/(.*)\/(.*)/ );
    }

    my $plugin_container = Bread::Board::Container->new( name => $plugin );

    my $family_class = ( $family_tree ? "${family_tree}::" : '' ) . $plugin;

    my ($service_class) = ( "Service::$family_class" =~ m/(.*)::/ );

    create_method $service_class, $plugin, sub {
        bless {
            services  => $services,
            container => {
                parent => $parent_container,
                this   => $plugin_container,
            },
          },
          "Service::$family_class";
    };

    $parent_container->add_sub_container($plugin_container);

    while ( my ( $service_name, $meta ) = each %$services ) {
        if ( $service_name eq '_include' || $service_name eq '_includes' ) {
            $meta = [$meta] if !UNIVERSAL::isa( $meta, 'ARRAY' );

          FIND_DIR: foreach my $dir (@lib_paths) {
                foreach my $child (@$meta) {
                    my $path = "$dir/$child";
                    $path =~ s/::/\//g;

                    $path =~ s/\/Service$//;

                    next if !-d $path;

                    $process_service_file->(
                        path             => $path,
                        plugin           => $family_class . "::$child",
                        parent_container => $plugin_container,
                        family_tree      => $family_class,
                    );
                }
            }
        }
        else {
            my $injection =
              exists $meta->{block}
              ? 'Bread::Board::BlockInjection'
              : 'Bread::Board::ConstructorInjection';

            if ($meta->{class}) {
                my $class = $meta->{class};
                $class =~ s/::/\//;
                require "$class.pm";
            }

            $plugin_container->add_service(
                $injection->new( name => $service_name, %$meta ) );

            create_method "Service::$family_class", $service_name, sub {
                my ( undef, %args ) = @_;
                $plugin_container->resolve(
                    service    => $service_name,
                    parameters => \%args,
                );
            };
        }
    }
};

my $MF = Bread::Board::Container->new( name => 'Manifestation' );

listdirs \@lib_paths => sub {
    my %row = @_;
    $process_service_file->(
        path             => $row{path},
        plugin           => $row{plugin},
        parent_container => $MF,
        family_tree      => '',
    );
  },
  {
    dir_only   => 1,
    die_no_dir => 0,
    alias      => 'plugin',
  };

no MF::Utils;

1;
