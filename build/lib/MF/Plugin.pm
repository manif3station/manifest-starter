package MF::Plugin;

use Moo;

use MF::Utils qw(
  cd
  defor
  load_yaml
  makedir
  makedirs
  openfile
  touchfile
  writefile
);

has name         => ( is => 'ro', required => 1, );
has base_dir     => ( is => 'ro', required => 1, );
has my_files_dir => ( is => 'ro', required => 1, );
has plugins_dir  => ( is => 'ro', required => 1, );

has plugin_dir => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my ($self) = @_;
        $self->my_files_dir . '/' . $self->name;
    },
);

has is_installed => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my ($self) = @_;
        -f $self->plugin_dir . '/installed'
    },
);

has is_enabled => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my ($self) = @_;
        return 0 if !$self->is_installed;
        return 1 if !-f $self->plugin_dir . '/disabled';
    },
);

has version => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my ($self) = @_;
        my $version = openfile $self->plugin_dir . '/version';
        return defor $version, 1;
    },
);

sub activate {
    my ($self) = @_;
    unlink $self->plugin_dir . '/disabled';
}

sub deactivate {
    my ($self) = @_;
    touchfile $self->plugin_dir . '/disabled';
}

my $get_file_ext = sub {    # Private Function
    my ($file) = @_;

    if ( $file =~ m/(.+)\.(tb[z]?2|tar\.b[z]?2|tgz|tar\.gz|txz|tar.xz)$/i ) {
        return ( $1, $2 );
    }
    else {
        return;
    }
};

sub install_from_url {
    my ( $self, %options ) = @_;

    my $url = defor $options{url}, $self->name
      or return;

    my ( $filename ) = ( $url =~ m/.*\/(.*)/ );

    $filename =~ s/\?.*//;
    $filename =~ s/\#.*//;

    return if !$get_file_ext->($filename);

    cd $self->plugins_dir => sub {
        my ($dir) = @_;

        system sprintf "curl -Lk %s > %s",
          map { quotemeta } ( $url, $filename );

        $self->install_from_archive(%options, archive => "$dir/$filename");
    };
}

sub install_from_archive {
    my ( $self, %options ) = @_;

    my $archive = defor $options{archive}, $self->name;

    if ( !-f $archive ) {
        $archive = $self->plugins_dir . "/$archive";
        if ( !-f $archive ) {
            die "Plugin Installation File is not found. ([x] $archive)";
        }
    }

    my ( $path, $plugin ) = ( $archive =~ m/(.*)\/(.*)/ );

    my ( $file_with_version, $ext ) = $get_file_ext->($archive)
      or return;

    my ( $plugin_dir, $ver ) = split /-/, $file_with_version;
    my ( $name ) = ( $plugin_dir =~ m/.*\/(.*)/ );

    if (!$ver) {
        warn "Plugin Archive File missing version. ([x] $plugin)";
        return;
    }

    $ver =~ s/[^\d\.]//g;
    $ver = defor $ver => '1.0';

    my $old_plugin = Service->MF->Plugins->plugin(name => $name);

    return system qw(rm), $archive
      if $old_plugin->is_installed
      && $old_plugin->version >= $ver
      && !$options{reinstall};

    my $switch = '-x';
    $switch .= 'z' if $ext =~ /gz/;
    $switch .= 'j' if $ext =~ /b[z]?2/;
    $switch .= 'x' if $ext =~ /xz/;
    $switch .= 'f';

    system qw(tar), $switch, $archive, -C => makedir $plugin_dir;

    if ( my $list = load_yaml file => "$plugin_dir/plugins.yml" ) {
        foreach my $url(@$list) {
            $self->install_from_url(url => $url, %options);
        }
    }

    writefile $self->my_files_dir . "/$name/version", content => $ver;

    system qw(mv -v), $archive, makedir $self->my_files_dir . "/$name";

    $self->install_from_plugins_dir(
        path   => $self->plugins_dir,
        plugin => $name,
    );
}

my $transfer_new_plugin_dir = sub {    # private method
    my ( $self, $where, $name, %options ) = @_;

    makedir $where if $options{mkdir};

    return '' if !-d $where;

    my $store = makedir $self->my_files_dir . "/$name/$where";

    system qw(rsync --delete -Pa --exclude stateful), "$where/", $store;

    system qw(rsync --ignore-existing -Pa), makedirs "$where/stateful/",
      $self->my_files_dir . "/$name/$where/stateful";

    my $target = makedir( $self->base_dir . "/$where/" ) . $name;

    symlink $store, $target;
};

my $clean_my_file_dir = sub {
    my ( $self, $name ) = @_;
    makedir 'empty_dir';
    system qw(rsync --delete -Pa --exclude stateful), "empty_dir/",
      $self->my_files_dir . "/$name";
};

my $transfer_new_plugin_file = sub {    # private method
    my ( $self, $file, $name ) = @_;
    return if !-f $file;
    system qw(cp -v), $file, makedir $self->my_files_dir . "/$name";
};

sub install_from_plugins_dir {
    my ( $self, %options ) = @_;

    my $path = $options{path} || $self->plugins_dir;
    my $name = $options{plugin}
      or die "Mising Plugin Name to install from plugins directory";

    my $plugin_dir = "$path/$name";

    die "Plugin Directory is not found. ([x] $plugin_dir)"
      if !-d $plugin_dir;

    print "Install Plugin: $name\n";

    my ( $copyd, $copyf ) =
      ( $transfer_new_plugin_dir, $transfer_new_plugin_file );

    cd $plugin_dir => sub {
        $self->$clean_my_file_dir($name);

        makedir $self->my_files_dir . "/$name/stateful";

        $self->$copyf( 'docker-compose.yml', $name );
        $self->$copyf( 'deb.packages',       $name );
        $self->$copyd( 'deb.files', $name );

        if ( -f $ENV{MF_DEB_LIST_FILE} ) {
            system qw(apt-get install -y), split /\n/,
              openfile $ENV{MF_DEB_LIST_FILE};
        }

        if ( -f 'cpanfile' ) {
            die "Env: PERL_CARTON_PATH is not defined."
              if !$ENV{PERL_CARTON_PATH};
            system qq{HOME=/tmp carton install};
            $self->$copyf( 'cpanfile',          $name );
            $self->$copyf( 'cpanfile.snapshot', $name );
        }

        $self->$copyd( views  => $name );
        $self->$copyd( public => $name );
        $self->$copyd( config => $name );
        $self->$copyd( bin    => $name );
        $self->$copyd( lib    => $name );
        $self->$copyd( data   => $name, mkdir => 1 );

        touchfile $self->my_files_dir . "/$name/installed";

    };

    system qw(rm -fr), $plugin_dir;

    return 1;
}

1;
