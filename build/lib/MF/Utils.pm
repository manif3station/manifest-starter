package MF::Utils;

use strict;
use warnings;
use Cwd qw(cwd);
use base 'Exporter';

our @EXPORT_OK = qw(
  add_lib
  argv
  cd
  create_method
  defor
  deforset
  env
  file
  folder
  hash2str
  listdir
  listdirs
  load_json
  load_yaml
  makedir
  makedirs
  mf_envs
  openfile
  opts
  run_command
  set_env_from_file
  template_file
  touchfile
  writefile
  writefile_json
);

my %MF_ENV = ();

sub add_lib {
    my ($dir) = @_;

    return $dir if !-d $dir;
    return $dir if grep { $_ eq $dir } @INC;

    push @INC, $dir;

    return $dir;
}

sub defor {
    my ( $default, $or ) = @_;
    return ( defined($default) && length($default) ) ? $default : $or;
}

sub deforset {
    return $_[0] = defor(@_);
}

sub env {
    my ($key) = @_;
    return defor $ENV{$key}, defor $MF_ENV{$key}, '';
}

sub mf_envs {
    if ( !%MF_ENV ) {
        foreach my $key ( keys %ENV ) {
            next if $key !~ /^MF_/;
            $MF_ENV{$key} = $ENV{$key};
        }
    }

    if ( !%MF_ENV ) {
        set_env_from_file("/web/mf.env");
    }

    return wantarray ? %MF_ENV : \%MF_ENV;
}

sub hash2str {
    my $hash    = shift @_;
    my %options = @_ if @_;

    my $quotemeta = $options{quotemeta};
    my $join      = defor( $options{join}, ' ' );

    my @pairs = '';

    foreach my $key ( sort keys %$hash ) {
        push @pairs, sprintf "%s=%s",
          map { $quotemeta ? quotemeta : $_ } ( $key, $hash->{$key} );
    }

    return join $join, @pairs;
}

sub listdirs {
    my $dirs    = shift;
    my $code    = shift;
    my $options = shift || {};

    die "$dirs is not ArrayRef" if !$dirs || !UNIVERSAL::isa( $dirs, 'ARRAY' );

    my %list =
      map { $_ => [ listdir( $_, $code, $options ) ] } grep { $_ } @$dirs;

    return wantarray ? %list : \%list;
}

sub listdir {
    my $dir     = shift or return;
    my $code    = shift;
    my $options = shift || {};

    $code = defor( $code, sub { } );

    my @list = ();

    if ( !-d $dir ) {
        if ( $options->{die_no_dir} ) {
            die "Dir: $dir is not exists";
        }
        return @list;
    }

    opendir( my $DIR, $dir );

    while ( my $item = readdir($DIR) ) {
        next if $item eq '.' || $item eq '..';

        my $path = "$dir/$item";

        next if $options->{dir_only}  && !-d $path;
        next if $options->{file_only} && !-f $path;

        my %item = ();

        if ( my $alias = $options->{alias} ) {
            $item{$alias} = $item;
        }
        else {
            $item{item} = $item;
        }

        my %info = (
            %item,
            dir  => $dir,
            path => $path,
            type => -f $path ? 'file' : 'dir',
            size => -s $path,
        );
        my $action = $code->(%info);

        next if !$action || $action eq 'next';

        push @list, \%info;

        last if $action eq 'last';
        redo if $action eq 'redo';
    }

    closedir($DIR);

    return @list;
}

sub makedirs {
    map { makedir($_) } @_;
}

sub makedir {
    my $dir    = shift;
    my $switch = 'p';
    $switch .= 'v' if env('DEV');
    unlink $dir if -f $dir;
    system qw(mkdir), "-$switch", $dir if !-d $dir;
    return $dir;
}

sub set_env_from_file {
    my ($file) = @_;

    my $fh = openfile(
        $file,
        return_fh   => 1,
        die_message => "env file is not found",
    );

    while ( my $line = <$fh> ) {
        next if !$line || $line =~ /^[\s\t]*#/;
        chomp $line;
        $line =~ s/\$[\{]?([^\}]+)[\}]?/env($1)/xg;
        $line =~ s/\$\w+/env($1)/xg;
        my ( $key, $val ) = split /=/, $line, 2;
        $ENV{$key} = $MF_ENV{$key} = $val;
    }
}

sub touchfile {
    my ($path) = @_;
    writefile( $path, append => 1, content => '' );
}

sub writefile {
    my ( $path, %options ) = @_;

    my $encode = defor( $options{encode}, '' );
    my $mode   = $options{append} ? '>>' : '>';

    if ( open my $fh, $mode . $encode, $path ) {
        return $fh if $options{return_fh};
        print $fh $options{content};
        return
            $options{return_path}    ? $path
          : $options{return_content} ? $options{content}
          :                            undef;
    }
    elsif ( my $die = $options{die_message} ) {
        die sprintf $die, $!;
    }
    elsif ( my $warn = $options{warn_message} ) {
        warn sprintf $warn, $!;
    }
}

sub writefile_json {
    my ( $path, %options ) = @_;

    my $data = $options{data}
      or return;

    require JSON;

    my $json = JSON->new->canonical;

    $json->pretty if $options{pretty} || $ENV{DEV};
    $json->utf8   if $options{utf8};

    writefile(
        $path, %options,
        encode  => $options{utf8} ? 'utf8' : '',
        content => $json->encode($data)
    );
}

sub openfile {
    my ( $path, %options ) = @_;

    my $fh;

    if (UNIVERSAL::isa($path, 'GLOB')) {
        $fh = $path;
    }
    else {
        my $encode = defor( $options{encode}, '' );
        open $fh, "<$encode", $path;
    }

    if ( $fh ) {
        return $fh if $options{return_fh};
        local $/;
        return <$fh>;
    }
    elsif ( my $die = $options{die_message} ) {
        die sprintf $die, $!;
    }
    elsif ( my $warn = $options{warn_message} ) {
        warn sprintf $warn, $!;
    }
    elsif ( $options{return_path_on_error} ) {
        return $path;
    }

    return;
}

sub load_yaml {
    my (%options) = @_;

    my $yaml;

    if ( my $file = delete $options{file} ) {
        $yaml = openfile( $file, %options )
          or return;
    }
    elsif ( $yaml = $options{yaml} ) { }
    else                             { return }

    require YAML;
    my $data = YAML::Load($yaml);

    return $options{want} ? $data->{$options{want}} : $data;
}

sub load_json {
    my (%options) = @_;

    my $json;

    if ( my $file = delete $options{file} ) {
        $json = openfile( $file, %options )
          or return;
    }
    elsif ( $json = $options{json} ) { }
    else                             { return }

    require JSON::PP;
    my $data = JSON::PP->new->utf8->decode($json);

    return $options{want} ? $data->{$options{want}} : $data;
}

sub create_method {
    my ( $class, $method, $code ) = @_;

    return if $class->can($method);

    no strict 'refs';

    *{ $class . '::' . $method } = $code;

    print "New method: ${class}::$method\n" if $ENV{DEV2};

    use strict;
}

sub cd {
    my ( $path, $code, %options ) = @_;

    return if !-d $path;

    my $original = cwd();

    chdir $path;

    eval { $code->($path) };

    my $error = $@;

    chdir $original;

    die $@ if $@;
}

sub opts {
    my %options;

    if ($#_ == 0 && UNIVERSAL::isa($_[0], 'HASH')) {
        %options = %{$_[0]};
    }
    else {
        %options = @_;
    }

    return wantarray ? %options : \%options;
}

my $find_stateful_item = sub {
    my ( $where, $plugin, @options ) = @_;

    my $options = opts(@options);

    my $file   = $options->{file};
    my $folder = $options->{folder};

    die "Can only load either folder or file"
      if $file && $folder;

    die "Missing file or folder to load"
      if !$file && !$folder;

    my $base = $ENV{"MF_${where}_DIR"};

    my $dir          = "$base/$plugin";
    my $stateful_dir = "$dir/stateful";

    my $item = defor( $file, $folder );
    my $type = $file ? 'file' : 'folder';
    my %got;

    if (
        ( $options->{new} && -d $stateful_dir )
        || (
            $type eq 'file'
            ? -f "$stateful_dir/$item"
            : -d "$stateful_dir/$item"
        )
      )
    {
        %got = (
            dirs   => [ $stateful_dir, $dir ],
            dir    => $stateful_dir,
            path   => "$stateful_dir/$item",
            plugin => "$plugin/stateful/$item",
            $type  => $item,
            status => 'stateful',
        );
    }
    elsif (( $options->{new} && -d $dir )
        || ( $type eq 'file' ? -f "$dir/$item" : -d "$dir/$item" ) )
    {
        %got = (
            dirs   => [ $stateful_dir, $dir ],
            dir    => $dir,
            path   => "$dir/$item",
            plugin => "$plugin/$item",
            $type  => $item,
            status => 'default',
        );
    }
    elsif ( $options->{stop_when_not_found} ) {
        die "$where $type is not found. ([x] $plugin/$item)";
    }

    return if !%got;

    return $options->{want} ? $got{$options->{want}} : \%got;
};

sub file {
    my ( $where, $plugin, $file, @options ) = @_;

    my %options = opts(@options);

    $options{file} = $file;

    $find_stateful_item->( $where, $plugin, %options );
}

sub folder {
    my ( $where, $plugin, $folder, @options ) = @_;

    my %options = opts(@options);

    $options{folder} = $folder;

    $find_stateful_item->( $where, $plugin, %options );
}

sub template_file {
    my ( $plugin, $file, @options ) = @_;

    $file .= '.tt'
      if $file !~ m/\.(tt)$/;

    return file( VIEWS => $plugin, $file, opts(@options), want => 'plugin' );
}

sub argv {
    return if !@ARGV;

    my %options = @_;

    my %mapping;
    my %default_value;
    my %is_list;

    while ( my ( $key, $switch ) = each %options ) {
        if (UNIVERSAL::isa($switch, 'HASH')) {
            $default_value{$key} = defor $switch->{default}, '';
            $is_list{$key} = $switch->{is_list};
            $switch = $switch->{switch} or die "Missing switch for $key";
        }
        if ( ref $switch ) {
            map { $mapping{$_} = $key } @$switch;
        }
        else {
            $mapping{$switch} = $key;
        }
        $mapping{"--$key"} = $key;
    }

    die "Missing key mapping" if !%mapping;

    my %got = ();

    for ( my $i = 0 ; $i <= $#ARGV ; $i++ ) {
        my $item = $ARGV[$i];

        if ( $item eq '--' ) {
            $got{o_} = [ @ARGV[ $i+1 .. $#ARGV ] ];
            last;
        }
        elsif ( $item =~ m/^(?:--(no[-]?|)([^-]{2,})|-([^-]))$/ ) {
            my ( $no, $long, $short ) = ( $1, $2, $3 );

            my $key;

            if    ( defined $long  && ( $key = $mapping{"--$long"} ) ) { }
            elsif ( defined $short && ( $key = $mapping{"-$short"} ) ) { }
            else {
                $key = 'o_' . defor( $long, $short );
            }

            next if !defined $key;

            my $val = defor( $ARGV[ $i + 1 ], '--the-end' );

            if ( $val =~ m/^[-]{1,2}/ ) {
                $val = $no ? 0 : 1;
            }

            deforset($val, $default_value{$key});

            if ($is_list{$key}) {
                push @{deforset($got{$key}, [])}, $val;
            }
            else {
                $got{$key} = $val;
            }
        }
    }

    $got{o_ARGV} = \@ARGV if %got;

    return wantarray ? %got : \%got;
}

sub run_command {
    my %options = @_;

    my $cmd     = defor( $options{exec}, $options{system} ) or return;
    my $args    = $options{args};
    my $chdir   = $options{chdir};
    my %env     = %{ defor( $options{env}, {} ) };
    my $capture = $options{capture};

    my %mf_env = mf_envs();

    my $env = hash2str(
        { %mf_env, %env },
        quotemeta => 1,
        join      => ' ',
    );

    my $run = "$env " if $env;

    $run .= $cmd;

    $args = defor( join( ' ', map { quotemeta } @$args ), '' );

    $run =~ s/\{\{args\}\}/$args/;

    print ">> $run\n" if defor( $ENV{DEV}, $env{DEV} );

    if ( $options{exec} ) {
        chdir $chdir if $chdir;
        exec $run;
    }

    my %captured;

    my $execute = sub {
        return system $run if !$capture;

        require Capture::Tiny;

        my ( $stdout, $stderr, @result ) =
          Capture::Tiny::capture( sub { system $run } );

        %captured = (
            stdout => $stdout,
            stderr => $stderr,
            result => \@result,
        );
    };

    $chdir ? cd( $chdir => $execute ) : $execute->();

    return if !$capture;

    return wantarray ? %captured : \%captured;
}

1;
