package MF::DockerCompose;

use strict;
use warnings;
use base 'Exporter';

our @EXPORT_OK = qw(
  exec_args
);

sub exec_args {
    (
        ( $ENV{_USER}  ? ( -u        => $ENV{_USER} )  : () ),
        ( $ENV{_CWD}   ? ( -w        => $ENV{_CWD} )   : () ),
        ( $ENV{_INDEX} ? ( '--index' => $ENV{_INDEX} ) : () ),
        ( $ENV{_STDIN} ? ('-T') : () ),
        _env_args(),
    )
}

sub _env_args {
    my @exec_args;

    while ( my ( $key, $val ) = each %ENV ) {
        if ( $key =~ m/^_ENV_(.+)/ ) {
            push @exec_args, -e => "$1=$val";
        }
    }

    return @exec_args;
}

1;
