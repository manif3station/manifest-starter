#!/usr/bin/env perl

use strict;
use warnings;
use MF::Utils qw(listdir openfile);

my $myfiles = $ENV{MF_MY_FILES};

exit if !-d $myfiles;

listdir $myfiles => sub {
    my %row = @_;

    my $plugin = $row{plugin};

    my $list = "$myfiles/$plugin/$ENV{MF_DEB_LIST_FILE}";

    return if !-f $list;

    system qw(apt-get install -y), split /\n/, openfile $list;

}, {
    dir_only => 1,
    alias    => 'plugin',
};
