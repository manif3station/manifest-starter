#!/usr/bin/env perl

use strict;
use warnings;

use MF::Utils qw(defor opts);

use local::lib defor $ENV{MF_CPAN_DIR}, '/tmp';

use MF::Services;

my ( $path, @args ) = @ARGV;

die "Which service are you try to access?" if !$path;

$path =~ s/(\.|\-\>)/::/g;

my ( $class, $func ) = ( $path =~ m/(.*)::(.*)/ );

die "Unknown service. (! $path)"
  if !$func;

$class = "Service::$class";

die "This isn't CLI Service. (! $class)"
  if $class !~ m/::CLI/;

my $service = $path;

$service =~ s/::/->/g;

die "Service $service is undefined (! $service)"
  if !UNIVERSAL::can( $class, $func );

my %options = opts(@args) if @args;

if ( $options{stdin_base64} ) {
    require MIME::Base64::Perl;
    $options{stdin} =
      MIME::Base64::Perl::decode_base64( delete $options{stdin_base64} );
}

$class->$func(%options);

exit;
