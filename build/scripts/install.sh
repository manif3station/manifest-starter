cd /tmp/setup

mv -v install-deb-packages.pl /usr/local/bin/install-deb-packages
mv -v plugin-cli              /usr/local/bin/plugin-cli
mv -v plugin-srv.pl           /usr/local/bin/Service
mv -v process-controller.pl   /usr/local/bin/process
mv -v create-new-project.pl   /usr/local/bin/start-here

#mv -v /usr/local/bin/carton   /usr/local/bin/carton.origin
#mv -v carton_ver              /usr/local/bin/carton

chmod 0755 -R /usr/local/bin

cd /tmp

rm -fr setup
