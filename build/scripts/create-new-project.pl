#!/usr/bin/env perl

use strict;
use warnings;

use MF::Utils qw(mf_envs writefile defor argv);

my %args = argv(
    empty    => '-e',
    stateful => '-s',
);

my %mf = mf_envs;

my $from = $mf{MF_NEW_PROJECT_DIR};
my $to   = $ENV{USER_NEW_PROJECT_DIR};

my @empty = $args{empty} ? ('--delete') : ();

my @stateful =
  defor( $args{stateful}, 1 )
  ? ( '--exclude', 'stateful' )
  : ();

system qw(rsync -Pa), @stateful, @empty, "$from/", $to;
system qw(rsync -Pa), @stateful, @empty, "$mf{MF_CORE_LIB_DIR}/", "$to/lib";

my $fh = writefile "$to/$mf{MF_ENV_FILE}", return_fh => 1;

foreach my $key ( sort keys %mf ) {
    printf $fh qq{%s=%s\n}, $key, $mf{$key};
}

system qw(chown -R), "$mf{MF_UID}:$mf{MF_GID}", $to;
system qw(chmod 0755), "$to/dock";
