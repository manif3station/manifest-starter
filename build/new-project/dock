#!/usr/bin/env perl

use strict;
use warnings;

use FindBin qw($Bin);
use lib "$Bin/lib";

use MF::Utils qw(
  defor
  env
  file
  hash2str
  listdir
  mf_envs
  run_command
  set_env_from_file
);

use MF::DockerCompose qw(exec_args);

my @DCs = qw(docker-compose.yml);

sub main {
    set_env_from_file "$Bin/mf.env";

    my $my_files = $Bin . env 'MF_MY_FILES';

    local $ENV{MF_MY_FILES_DIR} = $my_files;

    run() if !-d $my_files;

    listdir $my_files => sub {
        my %row = @_;

        my $plugin_dir = $row{path};

        return if -f "$plugin_dir/disabled";

        my $dc = file
          MY_FILES => $row{plugin},
          "docker-compose.yml", want => 'path'
          or return;

        system qw(sudo chown),
          join( ':', map { env "MF_$_" } qw(UID GID) ), $dc
          if env('MF_UID') != ( stat $dc )[4];

        push @DCs, $dc;
      },
      {
        dir_only => 1,
        alias    => 'plugin',
      };

    run();
}

sub run {
    my @dc_files = map { -f => $_ } @DCs;
    my @argv     = ( $ARGV[0], exec_args(), @ARGV[ 1 .. $#ARGV ] );

    run_command
      chdir => $Bin,
      exec  => 'docker-compose {{args}}',
      args  => [ @dc_files, @argv ],
      env   => {
        CWD => $Bin,
        PWD => $ENV{PWD},
      };
}

caller or main();
