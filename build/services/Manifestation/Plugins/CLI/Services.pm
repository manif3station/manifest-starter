package Manifestation::Plugins::CLI::Services;

use strict;
use warnings;
use MF::Utils qw(listdir);

sub services {
    {
        install => {
            parameters => {
                reinstall => { isa => 'Bool', default => 0 },
            },
            block => sub {
                my ($self) = @_;

                my %args = map { $_ => $self->param($_) } qw(reinstall);

                my $found_new_plugins;

                listdir $ENV{MF_PLUGINS_DIR} => sub {
                    my %row = @_;

                    $found_new_plugins =
                      Service->MF->Plugins->plugin( name => $row{plugin} )
                      ->install_from_archive(%args);
                  },
                  { file_only => 1, alias => 'plugin' };

                print "Done!\n" if $found_new_plugins;
            },
        },
        get => {
            parameters => {
                url       => { isa => 'Str' },
                reinstall => { isa => 'Bool', default => 0 },
            },
            block => sub {
                my ($self) = @_;

                my %args = map { $_ => $self->param($_) } qw(url reinstall);

                Service->MF->Plugins->plugin( name => $args{url} )
                  ->install_from_url(%args);
            },
        },
    }
}

1;
