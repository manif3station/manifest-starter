package Manifestation::Plugins::Services;

use strict;
use warnings;
use Bread::Board qw(literal);
use MF::Utils qw(makedir);

sub services {
    {
        plugin => {
            class      => 'MF::Plugin',
            parameters => {
                name         => { isa => 'Str' },
                base_dir => {
                    isa     => 'Str',
                    default => makedir $ENV{MF_BASE_DIR},
                },
                my_files_dir => {
                    isa     => 'Str',
                    default => $ENV{MF_MY_FILES},
                },
                plugins_dir => {
                    isa     => 'Str',
                    default => makedir $ENV{MF_PLUGINS_DIR},
                },
            },
        },
        _includes => [qw(
            Manifestation::Plugins::CLI
        )],
    }
}

1;
