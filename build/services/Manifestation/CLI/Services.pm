package Manifestation::CLI::Services;

use strict;
use warnings;

use MF::Utils qw(load_yaml);
use JSON;

sub services {
    {
        hello => {
            block => sub {
                my ($self) = @_;
                print "Hello\n";
                foreach my $i ( 1 .. 5 ) {
                    my $v = $self->param("arg$i")
                      or next;
                    print "arg $i: $v\n";
                }
            },
            parameters => {
                arg1 => { optional => 1 },
                arg2 => { optional => 1 },
                arg3 => { optional => 1 },
                arg4 => { optional => 1 },
                arg5 => { optional => 1 },
            },
        },
        yml2json => {
            block => sub {
                my ($self) = @_;

                my $data = load_yaml
                  file => $self->param('file'),
                  yaml => $self->param('stdin');

                print JSON->new->pretty->canonical->encode($data);
            },
            parameters => {
                file  => { isa => 'Str', optional => 1 },
                stdin => { isa => 'Str', optional => 1 },
            }
        },
    }
}

1;
