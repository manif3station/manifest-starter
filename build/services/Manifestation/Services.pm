package Manifestation::Services;

use strict;
use warnings;

sub container_name { 'MF' }

sub services {
    {
        env => {
            block => sub { \%ENV },
        },
        _includes => [qw(
            Manifestation::CLI
            Manifestation::Plugins
        )],
    },
}

1;
